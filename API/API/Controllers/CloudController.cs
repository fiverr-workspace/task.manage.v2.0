﻿using API.DatabaseAdapter.TokenValidation.Infrastructure.IServices;
using APIBusinessLogic.Infrastructure.IService;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CloudController : BaseController
    {
        private readonly ICloudService _cloudService;
        private readonly ITokenService _tokenService;
        public CloudController(ICloudService cloudService, ITokenService tokenService)
        {
            _cloudService = cloudService;
            _tokenService = tokenService;
        }

        [HttpPost]
        public async Task<IActionResult> SaveApplication(int applicationId)
        {
            await _cloudService.SaveApplicationAsync(applicationId);
            return null;
        }

        //[Authorize]
        //[HttpPost]
        //public async Task<IActionResult> GetApplications()
        //{
        //    await _cloudService.GetApplicationsAsync();
        //    return null;
        //}
    }
}
