﻿using API.DatabaseAdapter;
using API.DatabaseAdapter.Abstractions;
using API.DatabaseAdapter.Contexts;
using API.DatabaseAdapter.IdentityModel;
using API.DatabaseAdapter.TokenValidation.Infrastructure.IServices;
using API.DatabaseAdapter.TokenValidation.Infrastructure.Services;
using APIBusinessLogic.Infrastructure.IService;
using APIBusinessLogic.Infrastructure.Service;
using APIModel.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace API.Services
{
    public static class ServicesCollectionExtension
    {
        public static IServiceCollection AddInfrastructurServices(this IServiceCollection services)
        {
            services.AddScoped<ICloudService, CloudService>();
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<ITaskManagementAPIDbContext, TaskManagementAPIDbContext>();            
            //services.AddIdentity<ApplicationUser, IdentityRole>().AddEntityFrameworkStores<ApplicationDbContext>();
            return services;
        }


        public static IServiceCollection AddCustomConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<APIConfigurationSettings>(configuration.GetSection("APIConfigurationSettings"));
            return services;
        }
        public static IServiceCollection AddHttpContext(this IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            return services;
        }

        public static IServiceCollection AddCustomDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<TaskManagementAPIDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"), b =>
                {
                    b.MigrationsAssembly("API");
                });
            });

            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
            }).AddEntityFrameworkStores<TaskManagementAPIDbContext>().AddDefaultTokenProviders();
            return services;
        }

        //public static IServiceCollection AddDRXApiClient(this IServiceCollection services, IConfiguration configuration) {
        //    services.AddHttpClient<IDRXAPIClient, DRXAPIClient>(client => {
        //        client.BaseAddress = new Uri(configuration.GetValue<string>("SelectQuoteSettings:DRXSettings:BaseUri"));
        //    });
        //    return services;
        //}

        //public static IServiceCollection AddDRXAuthentication(this IServiceCollection services, IConfiguration configuration) {
        //    services.AddHttpClient<IAuthentication, Authentication>(client => {
        //        client.BaseAddress = new Uri(configuration.GetValue<string>("SelectQuoteSettings:DRXSettings:LinkAuthentication"));
        //    }).AddTransientHttpErrorPolicy(policy => policy.WaitAndRetryAsync(2, _ => TimeSpan.FromSeconds(2)));
        //    return services;
        //}

        //public static IServiceCollection AddQrsApiClient(this IServiceCollection services, IConfiguration configuration) {
        //    services.AddHttpClient("QrsApiClient", client => {
        //        client.BaseAddress = new Uri(configuration.GetValue<string>("SelectQuoteSettings:QrsSettings:BaseUri"));
        //        client.DefaultRequestHeaders.Add("x-api-key", configuration.GetValue<string>("SelectQuoteSettings:QrsSettings:QRSAPIKey"));
        //    });
        //    return services;
        //}
    }
}
