﻿using System;

namespace API.DatabaseAdapter.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name{ get; set; }
        public string Address { get; set; }
        public int PhoneNo { get; set; }
        public DateTime CreatedDate{ get; set; }
    }
}
