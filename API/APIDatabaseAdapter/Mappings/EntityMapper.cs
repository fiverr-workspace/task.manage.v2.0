﻿using Microsoft.EntityFrameworkCore;

namespace API.DatabaseAdapter.Mappings {
    internal class EntityMapper {
        public void Configure(ModelBuilder modelBuilder) {
            modelBuilder.ApplyConfiguration(new EmployeeMap());
        }
    }
}
