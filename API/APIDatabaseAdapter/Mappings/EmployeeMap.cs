﻿using API.DatabaseAdapter.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace API.DatabaseAdapter.Mappings
{
    public class EmployeeMap : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {

            builder.ToTable("Employee", "dbo");
            builder.HasKey(c => c.Id);

            // Column Mappings
            builder.Property(c => c.Id).HasColumnName("Id");

            builder.Property(c => c.Name).HasColumnName("Name");

            builder.Property(c => c.Address).HasColumnName("Address");

            builder.Property(c => c.PhoneNo).HasColumnName("PhoneNo");

            builder.Property(c => c.CreatedDate).HasColumnName("CreatedDate");

            // Relationships
        }
    }
}
