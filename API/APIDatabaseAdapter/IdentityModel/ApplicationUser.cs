﻿using API.DatabaseAdapter.TokenValidation.Infrastructure.Entities;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace API.DatabaseAdapter.IdentityModel
{
    //TODO: add appliction user model in api project
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<RefreshToken> RefreshTokens { get; set; }
    }
}
