﻿namespace API.DatabaseAdapter.TokenValidation.Infrastructure.Settings
{
    public class JWTConstants
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public double DurationInMinutes { get; set; }
    }
}
