﻿using API.DatabaseAdapter.IdentityModel;
using API.DatabaseAdapter.TokenValidation.JwtModels;
using System.Threading.Tasks;

namespace API.DatabaseAdapter.TokenValidation.Infrastructure.IServices
{
    public interface ITokenService
    {
        Task<string> RegisterAsync(RegisterModel model);
        Task<AuthenticationModel> GetTokenAsync(TokenRequestModel model);
        Task<string> AddRoleAsync(AddRoleModel model);

        Task<AuthenticationModel> RefreshTokenAsync(string jwtToken);

        bool RevokeToken(string token);
        ApplicationUser GetById(string id);
    }
}
