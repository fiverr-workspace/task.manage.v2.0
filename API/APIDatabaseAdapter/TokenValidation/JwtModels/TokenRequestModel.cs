﻿using System.ComponentModel.DataAnnotations;

namespace API.DatabaseAdapter.TokenValidation.JwtModels
{
    public class TokenRequestModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
