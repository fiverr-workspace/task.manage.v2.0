﻿namespace API.DatabaseAdapter.TokenValidation.JwtModels
{
    public class RevokeTokenRequest
    {
        public string Token { get; set; }
    }
}
