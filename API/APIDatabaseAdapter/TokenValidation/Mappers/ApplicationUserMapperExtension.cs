﻿using API.DatabaseAdapter.IdentityModel;

namespace JWT.Token.Security.Mappers
{
    public static class ApplicationUserMapperExtension
    {
        public static JWT.Token.Security.JwtModels.ApplicationUser ApplicationUserMapper(this ApplicationUser applicationUser)
        {
            JWT.Token.Security.JwtModels.ApplicationUser User = new JWT.Token.Security.JwtModels.ApplicationUser
            {
                FirstName = applicationUser.FirstName,
                LastName = applicationUser.LastName
            };
            return User;
        }
    }
}
