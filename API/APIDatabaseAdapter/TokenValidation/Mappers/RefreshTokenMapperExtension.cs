﻿using API.DatabaseAdapter.Contexts.Entities;

namespace JWT.Token.Security.Mappers
{
    public static class RefreshTokenMapperExtension
    {
        public static RefreshToken RefreshTokenMapper(this JWT.Token.Security.Infrastructure.Entities.RefreshToken refreshToken)
        {
            RefreshToken token = new RefreshToken
            {
                Token = refreshToken.Token,
                Expires = refreshToken.Expires,
                Created = refreshToken.Created,
                Revoked = refreshToken.Revoked,

            };
            return token;
        }
    }
}
