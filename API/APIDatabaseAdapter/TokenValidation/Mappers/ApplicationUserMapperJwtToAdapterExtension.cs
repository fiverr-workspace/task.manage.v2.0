﻿using System;
using System.Collections.Generic;
using System.Text;
using API.DatabaseAdapter.IdentityModel;

namespace JWT.Token.Security.Mappers
{
    public static class ApplicationUserMapperJwtToAdapterExtension
    {
        public static JWT.Token.Security.JwtModels.ApplicationUser ApplicationUserMapper(ApplicationUser applicationUser)
        {
            JWT.Token.Security.JwtModels.ApplicationUser User = new JWT.Token.Security.JwtModels.ApplicationUser
            {
                FirstName = applicationUser.FirstName,
                LastName = applicationUser.LastName
            };
            return User;
        }

    }
}
