﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace APIBusinessLogic.Infrastructure.IService
{
    public interface ICloudService
    {
        Task SaveApplicationAsync(int applicationId);
        Task GetApplicationsAsync();        
    }
}
